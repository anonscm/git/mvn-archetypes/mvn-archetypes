# language: de
Funktionalität: Beispiele


Szenario: Ein simples Beispiel

Wenn ich "1" und "2" addiere
Dann erhalte ich "3"


@wip
Szenario: Ein simples Beispiel (Fehlschlag)

Wenn ich "2" und "2" addiere
Dann erhalte ich "0"

	
Szenario: Überprüfung des Datensatz Inhaltes

Angenommen ich konvertiere folgende Datentabelle:
	|	Vorname	|	Nachname	|	Beruf				|
	|	Sven	|	Schumann	|	Softwareentwickler	|
	|	Ulf		|	Knödel		|	Koch				|
	|	Berta	|	Buschwald	|	Försterin			|
Wenn "3" Personen eingelesen wurden
Dann ist "Sven" von Beruf "Softwareentwickler"
Dann ist "Ulf" von Beruf "Koch"
Dann ist "Berta" von Beruf "Försterin"
