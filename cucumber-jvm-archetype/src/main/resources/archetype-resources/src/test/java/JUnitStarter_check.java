package ${package};

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;
import ${package}.CucumberJUnitRunner.ExtendedOptions;

@RunWith(CucumberJUnitRunner.class)
@Cucumber.Options(
	//this code will only look into "features/" folder for features
	features={"classpath:features/"},
	
	//DO NOT REMOVE THIS FORMATTER!
	format={"${package}.formatter.RuntimeInfoCatcher"},
	
	monochrome=true,
	
	dryRun=true
)
@ExtendedOptions(
	executeHooks = false
)
public class JUnitStarter_check {

	/*******************************************************
	 * DO NOT ADD SOME METHODS! THIS CLASS MUST BE EMPTY!!! 
	 *******************************************************/
}
