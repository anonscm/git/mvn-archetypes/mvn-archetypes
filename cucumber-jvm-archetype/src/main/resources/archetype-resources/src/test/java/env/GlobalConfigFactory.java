package ${package}.env;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class GlobalConfigFactory {
	public static final String CLASSPATH_PREFIX = "classpath:";
	public static final File USED_CONFIG_FILE = new File(System.getProperty("user.dir") + "/used-cucumber-config.json");
    
	/**
     * new configuration instance populated one or more JSON resources. <br>
     * <br>
     * The given JSON sources are merged recursively with sources further right in the list overriding those further
     * left in the list in case of conflicting settings.
     * 
     * Each source should be a valid JSON String or an URL. In addition to the protocols supported by the vm, this method supports 
     * classpath:/... URLs.
     * 
     * 
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public GlobalConfig fromJson(GlobalConfig defaultValues, String... sources) throws JsonParseException, JsonMappingException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode tree = mapper.createObjectNode();
        
        if(defaultValues != null){
        	merge(tree, mapper.readTree(mapper.writeValueAsString(defaultValues)));
        }
        
        for (String source : sources) {
            // XXX: would be nicer to register a URLStreamHandler, but I could not think of a simple and robust way to
            // do this --ld.
            if (source.startsWith(CLASSPATH_PREFIX)) {
                processClasspathFile(mapper, tree, source);
            } else {
                processOtherFile(mapper, tree, source);
            }   
        }
        
        GlobalConfig config = mapper.readValue(tree, GlobalConfig.class);
        writeConfig(mapper, config);
        
        return config;
    }

	private void writeConfig(ObjectMapper mapper, GlobalConfig config) throws JsonGenerationException, JsonMappingException, IOException {
		mapper.writerWithDefaultPrettyPrinter().writeValue(USED_CONFIG_FILE, config);
	}

	private void processOtherFile(final ObjectMapper mapper, final JsonNode tree, String source) throws IOException, JsonProcessingException {
		try {
		    final URL url = new URL(source);                    
		    try{
		        merge(tree, mapper.readTree(url));
		    }catch(FileNotFoundException fne){
		        ;//simply ignore missing files
		    }
		} catch (MalformedURLException e) {
		    // one more try: maybe it isn't an url at all, but the json data was provided inline?
		    merge(tree, mapper.readTree(source));
		}
	}

	private void processClasspathFile(final ObjectMapper mapper, final JsonNode tree, String source) throws IOException, JsonProcessingException {
		final String resourceName = source.substring(CLASSPATH_PREFIX.length());
		final URL url = GlobalConfigFactory.class.getClassLoader().getResource(resourceName);
		// skip missing resources
		if(url!=null){                    
		    merge(tree, mapper.readTree(url));
		}
	}
    
    /**
     * Recursively merges the contents of updateNode into mainNode.
     * @param mainNode the node being updated. Will be modified.
     * @param updateNode the node containing the overrides
     * @return mainNode
     */
    //code found here http://stackoverflow.com/a/11459962
    //and adopted to our needs. 
    public JsonNode merge(JsonNode mainNode, JsonNode updateNode) {

		Iterator<String> fieldNames = updateNode.getFieldNames();
		while (fieldNames.hasNext()) {
			String fieldName = fieldNames.next();
			JsonNode jsonNode = mainNode.get(fieldName);
			// if field doesn't exist or is an embedded object
			if (jsonNode != null && jsonNode.isObject()) {
				merge(jsonNode, updateNode.get(fieldName));
			} else {
				if (mainNode instanceof ObjectNode) {
					// Overwrite field
					JsonNode value = updateNode.get(fieldName);
					((ObjectNode) mainNode).put(fieldName, value);
				}
			}
		}

		return mainNode;
    }
}
