package ${package};

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.runners.model.InitializationError;

import cucumber.api.junit.Cucumber;

/**
 * A seperate Cucumber-Runner, so that we can get
 * the used Option-Annotation.
 * 
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public class CucumberJUnitRunner extends Cucumber {
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.TYPE})
	public static @interface ExtendedOptions{
		/**
		 * @return false if the hooks should not be executed.
		 */
		boolean executeHooks() default true;
	}
	
	@Cucumber.Options
	@ExtendedOptions
	class AnnotationHost{}
	
	private static Cucumber.Options usedOptions;
	private static ExtendedOptions usedExtOptions;
	
	public CucumberJUnitRunner(Class<?> clazz) throws InitializationError, IOException {
		super(clazz);
		
		usedOptions = (Cucumber.Options)clazz.getAnnotation(Cucumber.Options.class);
		if(usedOptions == null){
			usedOptions = (Cucumber.Options)AnnotationHost.class.getAnnotation(Cucumber.Options.class);
		}
		
		usedExtOptions = (ExtendedOptions)clazz.getAnnotation(ExtendedOptions.class);
		if(usedExtOptions == null){
			usedExtOptions = (ExtendedOptions)AnnotationHost.class.getAnnotation(ExtendedOptions.class);
		}
	}
	
	/**
	 * If cucumber will be executed via console, this
	 * class wont be instantiated. This behavior can
	 * be used to check if we are in a JUnit test.
	 * 
	 * @return
	 */
	public static boolean isJunitTest(){
		if(usedOptions == null) return false;
		else return true;
	}
	
	public static boolean isExecuteHooks(){
		return usedExtOptions.executeHooks();
	}
	
	public static boolean isDryRun(){
		return usedOptions.dryRun();
	}
	
	public static boolean isStrict(){
		return usedOptions.strict();
	}
	
	public static String[] getFeatures(){
		return usedOptions.features();
	}
	
	public static String[] getGlue(){
		return usedOptions.glue();
	}

	public static String[] getTags(){
		return usedOptions.tags();
	}
	
	public static String[] getFormat(){
		return usedOptions.format();
	}
	
	public static boolean isMonochrome(){
		return usedOptions.monochrome();
	}
	
	public static String[] getOptionName(){
		return usedOptions.name();
	}
	
}