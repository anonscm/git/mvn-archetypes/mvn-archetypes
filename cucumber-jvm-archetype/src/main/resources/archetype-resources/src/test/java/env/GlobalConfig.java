package ${package}.env;

/**
 * You can put your global properties in this bean.
 * 
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public class GlobalConfig {
	private static GlobalConfig instance;
	
	private String archivePath;
	private String archiveZipPath;
	
	public GlobalConfig(){
		instance = this;
	}
	
	public static GlobalConfig getInstance(){
		return instance;
	}
	
	public String getArchivePath() {
		return archivePath;
	}
	public void setArchivePath(String archivePath) {
		this.archivePath = archivePath;
	}
	public String getArchiveZipPath() {
		return archiveZipPath;
	}
	public void setArchiveZipPath(String archiveZipPath) {
		this.archiveZipPath = archiveZipPath;
	}
}
