package ${package}.env.event;

import cucumber.api.Scenario;

public interface HandlerAfter {

	/**
	 * This will be called after each scenario.
	 * 
	 * @param result
	 */
	public void handleAfterScenario(Scenario result);
}
