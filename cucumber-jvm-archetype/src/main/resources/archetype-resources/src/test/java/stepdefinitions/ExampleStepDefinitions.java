package ${package}.stepdefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import ${package}.entity.ExampleState;
import ${package}.entity.ExamplePerson;

/**
 * For each scenario, a NEW step-def-object is created!
 *  
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public class ExampleStepDefinitions extends AbstractStepDefinition{
	@Autowired ExampleState state;
	
	@Before
	public void before(){
		//such like the @Before-Annotation in JUnit
	}
	
	@After
	public void after(){
		//such like the @After-Annotation in JUnit
	}
	
	@Wenn("^ich \"([^\"]*)\" und \"([^\"]*)\" addiere$")
    public void ich_und_addiere(long l1, long l2) {
		state.setValue1(l1);
		state.setValue2(l2);
    }
	
	@Dann("^erhalte ich \"([^\"]*)\"$")
    public void erhalte_ich(long expected) {
		long result = state.getValue1() + state.getValue2();
		
        assertTrue(result == expected);
    }
	
	@Angenommen("^ich konvertiere folgende Datentabelle:$")
	public void ich_konvertiere_folgende_Datentabelle(List<ExamplePerson> personen) {
		state.setPersons(personen);
	}

	@Wenn("^\"([^\"]*)\" Personen eingelesen wurden$")
	public void n_Personen_eingelesen_wurden(Integer n) {
		assertTrue(state.getPersons().size() == n);
	}

	@Dann("ist \"([^\"]*)\" von Beruf \"([^\"]*)\"")
	public void ist_von_Beruf(String vorname, String beruf) {
		for (ExamplePerson curPerson : state.getPersons()) {
			if (curPerson.getVorname().equals(vorname)) {
				assertEquals(curPerson.getBeruf(), beruf);
				break;
			}
		}
	}
}
