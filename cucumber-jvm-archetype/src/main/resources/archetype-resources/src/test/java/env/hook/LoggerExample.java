package ${package}.env.hook;

import org.apache.log4j.Logger;

import cucumber.api.Scenario;

import ${package}.env.HookRegistry;
import ${package}.env.event.HandlerAfter;
import ${package}.env.event.HandlerBefore;
import ${package}.env.event.HandlerStart;
import ${package}.env.event.HandlerStop;

public class LoggerExample implements HandlerAfter, HandlerBefore, HandlerStart, HandlerStop{
	private static final Logger log = Logger.getLogger(LoggerExample.class);
	
	public LoggerExample() {
		//Register me
		HookRegistry.addOnAfterHandler(this);
		HookRegistry.addOnBeforeHandler(this);
		HookRegistry.addOnStartHandler(this);
		HookRegistry.addOnStopHandler(this);
	}
	
	@Override
	public void handleStop() {
		log.info("Cucumber finishs");
	}

	@Override
	public void handleStart() {
		log.info("Cucumber starts");
	}

	@Override
	public void handleBeforeScenario() {
		log.info("Before");
	}

	@Override
	public void handleAfterScenario(Scenario result) {
		log.info("After");
	}

}
