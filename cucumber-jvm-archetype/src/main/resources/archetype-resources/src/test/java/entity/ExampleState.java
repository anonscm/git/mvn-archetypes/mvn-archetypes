package ${package}.entity;

import java.util.List;

public class ExampleState {
	private Long value1;
	private Long value2;
	private List<ExamplePerson> persons;
	
	public Long getValue1() {
		return value1;
	}
	public void setValue1(Long value1) {
		this.value1 = value1;
	}
	public Long getValue2() {
		return value2;
	}
	public void setValue2(Long value2) {
		this.value2 = value2;
	}
	public List<ExamplePerson> getPersons() {
		return persons;
	}
	public void setPersons(List<ExamplePerson> persons) {
		this.persons = persons;
	}
}
