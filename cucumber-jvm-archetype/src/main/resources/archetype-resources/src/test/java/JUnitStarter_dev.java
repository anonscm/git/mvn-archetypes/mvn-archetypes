package ${package};

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(CucumberJUnitRunner.class)
@Cucumber.Options(
	//this code will only look into "features/" folder for features
	features={"classpath:features/"},
	
	//this code will be run only scenarios which annotated with "@wip"
	tags={"@wip"},
	
	//DO NOT REMOVE THIS FORMATTER!
	format={"${package}.formatter.RuntimeInfoCatcher"},
	
	monochrome=true
)
public class JUnitStarter_dev {

	/*******************************************************
	 * DO NOT ADD SOME METHODS! THIS CLASS MUST BE EMPTY!!! 
	 *******************************************************/
}
