package ${package};

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(CucumberJUnitRunner.class)
@Cucumber.Options(
	//this code will only look into "features/" folder for features
	features={"classpath:features/"},
	
	//this code will be run only scenarios which is not annotated with "@ignore"
	tags={"~@ignore"},
	
	//DO NOT REMOVE THIS FORMATTER!
	format={"${package}.formatter.RuntimeInfoCatcher"},
	
	monochrome=true
)
public class JUnitStarter_int {

	/*******************************************************
	 * DO NOT ADD SOME METHODS! THIS CLASS MUST BE EMPTY!!! 
	 *******************************************************/
}
