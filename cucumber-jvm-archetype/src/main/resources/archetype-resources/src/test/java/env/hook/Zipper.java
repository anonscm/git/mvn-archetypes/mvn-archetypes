package ${package}.env.hook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ${package}.env.HookRegistry;
import ${package}.env.event.HandlerStop;

/**
 * Diese Klasse sorgt dafür, dass am Ende von Cucumber
 * das entsprechende Archiv-Verzeichnis gepackt wird.
 *  
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class Zipper implements HandlerStop {
	private final String archiveDir;
	private final String zipFile;

	public Zipper(String archiveDir, String zipFile) {
		this.archiveDir = archiveDir;
		this.zipFile = zipFile;
		
		HookRegistry.addOnStopHandler(this);
	}
	
	@Override
	public void handleStop() {
		if(!isArchiveDirGiven() || !isZipGiven()) return; //nothing to do
		
		File targetDir = new File(archiveDir);
		targetDir.mkdirs();
		
		try {
			zipDirectory(targetDir.getAbsolutePath(), zipFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isArchiveDirGiven(){
		return 	archiveDir != null
				&&	!"".equals(archiveDir.trim())
				&&	new File(archiveDir).isDirectory();
	}
	
	private boolean isZipGiven(){
		return 	zipFile != null
				&& !"".equals(zipFile.trim());
	}
	
	private void zipDirectory(String dirPath, String filePath) throws IOException{
		new File(filePath).delete();
		
		zipDirectory(new File(dirPath), new File(filePath));
	}
	
	private void zipDirectory(File directory, File zip)
			throws IOException {
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zip));
		zos.setLevel(9);
		zip(directory, directory, zos);
		zos.close();
	}

	private void zip(File directory, File base, ZipOutputStream zos)
			throws IOException {
		File[] files = directory.listFiles();
		byte[] buffer = new byte[8192];
		int read = 0;
		for (int i = 0, n = files.length; i < n; i++) {
			if (files[i].isDirectory()) {
				zip(files[i], base, zos);
			} else {
				FileInputStream in = new FileInputStream(files[i]);
				ZipEntry entry = new ZipEntry(files[i].getPath().substring(
						base.getPath().length() + 1));
				zos.putNextEntry(entry);
				while (-1 != (read = in.read(buffer))) {
					zos.write(buffer, 0, read);
				}
				in.close();
			}
		}
	}
}
