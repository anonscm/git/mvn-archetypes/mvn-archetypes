# language: de
# Funktionalität: Google Suche
# Heute mal eine Tiefensuche.
Funktionalität: Bing Suche
Suche nach Windows mit www.bing.de

  Szenario: Finde alles zum Thema "Tiefen".
#Angenommen ich öffne "http://www.google.de"
#Angenommen ich öffne "http://www.google.de/#q=Tiefen"
#Und suche nach "Tiefen"
#Dann befindet sich "Hirnstimulation" unter den Ergebnissen

Angenommen ich öffne "http://www.bing.de"
Und suche nach "Windows"
Dann befindet sich "Microsoft" unter den Ergebnissen