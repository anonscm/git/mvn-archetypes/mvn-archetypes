# encoding: utf-8
begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end
require 'cucumber/formatter/unicode'
require "rubygems"

# Demonstrating that nothing stops you from mixing step definitions in different languages
# even in a single scenario -- provided you find a way to share your session state between
# the different implementations. See env.rb and SeleniumSingleton.java for one  
# example of how to do this with Java, Spring and JRuby.

Dann /^BROKENbefindet sich "([^"]*)" unter den Ergebnissen$/ do |erwartetesErgebnis| # absichtlich BROKEN eingefügt  weil step definitions aus bing_stepss.rb benutzt werden sollen
  # results are loaded using AJAX magic. We wait for at least one result before we continue.
  # browser.waitForCondition "selenium.isElementPresent(\"xpath=//ol[@id='rso']/li\")", "10000"
  browser.wait_for_page_to_load "30000"
  if not browser.is_text_present?(erwartetesErgebnis) then
    puts browser.body_text
    raise "Taucht nicht auf: "+erwartetesErgebnis 
  end
end

Angenommen /^BROKENich öffne "([^"]*)"$/ do |arg1| # absichtlich BROKEN eingefügt weil step definitions aus bing_stepss.rb benutzt werden sollen
  browser.open arg1
  browser.wait_for_page_to_load "30000"
end

Angenommen /^BROKENsuche nach "([^"]*)"$/ do |arg1| # absichtlich BROKEN eingefügt  weil step definitions aus bing_stepss.rb benutzt werden sollen
  #browser.type("xpath=//input[@title='Suche']" ,"#{arg1}")
  #browser.type("dom=document.forms[0]" ,"#{arg1}")
  #browser.click("xpath=//input[@type='submit']")
  #browser.click("xpath=/html/body/div[2]/div[1]/div[1]/div[2]/div[2]/div/form/div[1]/button[1]")
  
  browser.type("xpath=//input[@title='Suchbegriff eingeben']" ,"#{arg1}")
  browser.click("xpath=//input[@type='submit']")
  
  #browser.type("xpath=//input[@id='searchInput']" ,"#{arg1}")
  #browser.click("xpath=//input[@type='submit']")
  
end