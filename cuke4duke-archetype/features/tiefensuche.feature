# language: de

Funktionalität: Bing Suche
Suche nach Windows mit www.bing.de

  Szenario: Finde alles zum Thema "Windows".

Angenommen ich öffne "http://www.bing.de"
Und suche nach "Windows"
Dann befindet sich "Microsoft" unter den Ergebnissen