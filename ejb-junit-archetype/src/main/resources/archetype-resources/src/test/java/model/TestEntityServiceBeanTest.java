package ${package}.model;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import ${package}.model.TestEntity;
import ${package}.model.TestEntityServiceBeanTest;
import ${package}.service.TestEntityServiceBeanLocal;

public class TestEntityServiceBeanTest extends TestCase {

	private static Logger log = Logger.getLogger(TestEntityServiceBeanTest.class);

	private TestEntityServiceBeanLocal service;

	private TestEntity c1;
	
	private TestEntity c2;
	
	private TestEntity c3;
	
	@Before
	public void setUp() {
		Properties p = new Properties();
		p.put(Context.INITIAL_CONTEXT_FACTORY,
				"org.apache.openejb.client.LocalInitialContextFactory");
		
		// override defaults in persistence.xml with the test database connection
		p.put("openejb.configuration", "src/test/resources/openejb.xml");
		p.put("openejb.validation.output.level", "VERBOSE");
		p.put("testunit.jta-data-source", "java:/ejbunit-test");
		p.put("testunit.hibernate.dialect", "org.hibernate.dialect.DerbyDialect");
		p.put("testunit.hibernate.hbm2ddl.auto", "create-drop");
		
		try {
			InitialContext ctx = new InitialContext(p);
			service = (TestEntityServiceBeanLocal) ctx.lookup("TestEntityServiceBeanLocal");
		} catch (NamingException e) {
			log.error(e);
		}

		c1 = new TestEntity("Test1");
		c2 = new TestEntity("Test2");
		c3 = new TestEntity("Test3");
		
		c3.setParent(c2);
		c2.setParent(c1);
		
		service.add(c1);
		service.add(c2);
		service.add(c3);
	}

	@Test
	public void testGetById() {
		TestEntity t2 = service.getById(c2.getId());
		assertNotNull(t2);
		assertEquals(t2.getId(), c2.getId());
		
		TestEntity t1 = service.getById(c2.getParent().getId());
		assertNotNull(t1);
		assertEquals(t1.getId(), c1.getId());
	}

}