package ${package}.service.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ${package}.model.TestEntity;
import ${package}.service.TestEntityServiceBeanLocal;

@Stateless
public class TestEntityServiceBean implements TestEntityServiceBeanLocal {

	@PersistenceContext(unitName = "testunit")
	private EntityManager em;

	@Override
	public TestEntity getById(int id) {
		return em.find(TestEntity.class, id);
	}

	@Override
	public void add(TestEntity c) {
        em.persist(c);
	}
	
}
