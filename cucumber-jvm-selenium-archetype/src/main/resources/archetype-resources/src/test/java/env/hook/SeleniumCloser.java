package ${package}.env.hook;

import ${package}.env.SeleniumFactory;
import ${package}.env.HookRegistry;
import ${package}.env.event.HandlerStop;

/**
 * This class represents a hook. 
 * They destroy all selenium-instances at the shutdown.
 */
public class SeleniumCloser implements HandlerStop {

	public SeleniumCloser(){
		//register me
		HookRegistry.addOnStopHandler(this);
	}
	
	public void handleStop() {
		SeleniumFactory.closeAll();
	}
}
