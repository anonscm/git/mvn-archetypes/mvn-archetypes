package ${package}.env.hook;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import ${package}.env.GlobalConfig;
import ${package}.env.HookRegistry;
import ${package}.env.Utils;
import ${package}.env.event.HandlerStop;

/**
 * This class represents a hook.
 * They zip all test results to a separate zip-file.
 */
public class ResultZipper implements HandlerStop {
	@Autowired GlobalConfig config;
	
	public ResultZipper(){
		//register me
		HookRegistry.addOnStopHandler(this);
	}
	
	public void handleStop() {
		if(	config.getResultContentPath() != null &&
			!config.getResultContentPath().equals("") &&
			config.getResultContentZipPath() != null &&
			!config.getResultContentZipPath().equals("")){
			
			System.err.println("=============== Zip result-files ===============");
			try {
				Utils.zipDirectory(config.getResultContentPath(), config.getResultContentZipPath());
			} catch (IOException e) {
				System.err.println("Error on creating zip-file");
				e.printStackTrace();
			}
		}
	}

}
