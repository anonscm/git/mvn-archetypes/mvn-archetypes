package ${package}.env;

import java.util.concurrent.TimeUnit;

public class GlobalConfig {
	private static GlobalConfig instance;
	
	private String serverHost;
	private int port;
	private String baseURL;
	private String browserMode;
	private String htmlPath;
	private boolean screenshotEmbedded = true;
	private String screenshotPath;
	private String resultContentPath;
	private String resultContentZipPath;
	private long sleepTime;
	private TimeUnit sleepUnit;
	private boolean newSessionPerFeature = false;
	
	public GlobalConfig(){
		instance = this;
	}
	
	public static GlobalConfig getInstance(){
		return instance;
	}
	
	public String getServerHost() {
		return serverHost;
	}
	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getBaseURL() {
		return baseURL;
	}
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}
	public String getBrowserMode() {
		return browserMode;
	}
	public void setBrowserMode(String browserMode) {
		this.browserMode = browserMode;
	}
	public String getResultContentPath() {
		return resultContentPath;
	}
	public void setResultContentPath(String resultContentPath) {
		this.resultContentPath = resultContentPath;
	}
	public String getResultContentZipPath() {
		return resultContentZipPath;
	}
	public void setResultContentZipPath(String resultContentZipPath) {
		this.resultContentZipPath = resultContentZipPath;
	}
	public String getHtmlPath() {
		return htmlPath;
	}
	public void setHtmlPath(String htmlPath) {
		this.htmlPath = htmlPath;
	}
	public String getScreenshotPath() {
		return screenshotPath;
	}
	public void setScreenshotPath(String screenshotPath) {
		this.screenshotPath = screenshotPath;
	}
	public long getSleepTime() {
		return sleepTime;
	}
	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}
	public TimeUnit getSleepUnit() {
		return sleepUnit;
	}
	public void setSleepUnit(TimeUnit sleepUnit) {
		this.sleepUnit = sleepUnit;
	}
	public boolean isScreenshotEmbedded() {
		return screenshotEmbedded;
	}
	public void setScreenshotEmbedded(boolean screenshotEmbedded) {
		this.screenshotEmbedded = screenshotEmbedded;
	}
	public boolean isNewSessionPerFeature() {
		return newSessionPerFeature;
	}
	public void setNewSessionPerFeature(boolean newSessionPerFeature) {
		this.newSessionPerFeature = newSessionPerFeature;
	}
}
