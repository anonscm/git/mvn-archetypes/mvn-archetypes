package ${package}.env.hook;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.Scenario;
import ${package}.env.GlobalConfig;
import ${package}.env.HookRegistry;
import ${package}.env.Utils;
import ${package}.env.event.HandlerAfter;
import ${package}.formatter.RuntimeInfoCatcher;

/**
 * This class represents a hook.
 * They take the html-source after a step failed.
 */
public class HTMLSaver implements HandlerAfter {
	@Autowired WebDriver browser;
	@Autowired GlobalConfig config;
	
	public HTMLSaver(){
		//register me
		HookRegistry.addOnAfterHandler(this);
	}
	
	public void handleAfterScenario(Scenario result) {
		RuntimeInfoCatcher catcher = RuntimeInfoCatcher.getInstance();
		if(catcher == null){
			//this can be happen if the formatter is not configured
			//or the test is running in a IDE-JUnit-Test.
			return;
		}
		
		if(result.isFailed()){
			////
			//save html-result
			////
			if(	config.getHtmlPath() != null &&
				!config.getHtmlPath().equals("")){
				
				String htmlPath = Utils.generateOutputPath(
						config.getHtmlPath(), 
						catcher.getCurrentFeature(), 
						catcher.getCurrentScenarioCount(),
						catcher.getCurrentStepCount());
				htmlPath += ".html";
				
				Utils.createDirectoryIfDoesntExist(htmlPath);
				
				String htmlSource = browser.getPageSource();
				try {
					Utils.writeHtmlFile(htmlSource, htmlPath);
				} catch (IOException e) {
					System.err.println("Error on catching html-source.");
					e.printStackTrace();
				}
			}
		}
	}

}
