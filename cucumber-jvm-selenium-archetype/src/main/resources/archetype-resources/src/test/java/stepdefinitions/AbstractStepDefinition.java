package ${package}.stepdefinitions;

import org.springframework.test.context.ContextConfiguration;

/**
 * The only need of this class is the {@link ContextConfiguration}-Annotation.
 * If you dont want to extends from this class, then you must put
 * this annotation on your class! Or you will get spring-exceptions...
 * 
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
@ContextConfiguration("classpath:cucumber.xml")
public abstract class AbstractStepDefinition {

}
