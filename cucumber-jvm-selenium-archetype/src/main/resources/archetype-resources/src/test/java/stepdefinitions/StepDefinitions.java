package ${package}.stepdefinitions;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Und;
import cucumber.api.java.de.Wenn;

import ${package}.env.GlobalConfig;

public class StepDefinitions extends AbstractStepDefinition {
	@Autowired WebDriver browser;
	@Autowired GlobalConfig config;
	
	/********************************
	 * YOU CAN PUT YOUR STEPDEFINITIONS HERE
	 ********************************/
}
