package ${package}.env;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import ${package}.selenium.RemoteWebDriverWithScreenshotFunctionality;
import ${package}.CucumberJUnitRunner;

public class SeleniumFactory {
	private static Map<String, WebDriver> alwaysCreated = new HashMap<String, WebDriver>();
	
	protected final boolean executeHooks;
	
	/**
	 * This constructor will be invoked by spring.
	 */
	public SeleniumFactory(String executeHooks){
		if(CucumberJUnitRunner.isJunitTest()){
			this.executeHooks = CucumberJUnitRunner.isExecuteHooks();
		}else{
			this.executeHooks = Boolean.valueOf(executeHooks);
		}
	}
	
	public WebDriver buildRemote(String host, int port, String browserMode){
		return buildRemote(host, port, browserMode, 10, TimeUnit.SECONDS);
	}
	
	public WebDriver buildRemote(String host, int port, String browserMode, long sleepTime, TimeUnit sleepUnit){
		return buildRemote(host, port, browserMode, sleepTime, sleepUnit, true);
	}
	
	public WebDriver buildRemote(String host, int port, String browserMode, long sleepTime, TimeUnit sleepUnit, boolean loadFromCache){
		if(!executeHooks) return null;
		
		DesiredCapabilities capabilities = null;
		
		String uniqueId = generateUniqueString(host, port, browserMode);
		if(loadFromCache){
			for(String curKey : alwaysCreated.keySet()){
				if(curKey.startsWith(uniqueId)){
					return alwaysCreated.get(curKey);
				}
			}
		}
		
		if(browserMode.toLowerCase().startsWith("ff")){
			//firefox
			capabilities = DesiredCapabilities.firefox();
		}else if(browserMode.toLowerCase().startsWith("ie")){
			//InternetExplorer
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		}else if(browserMode.toLowerCase().startsWith("htmlunit")){
			//HTML-Unit (with javascript)
			capabilities = DesiredCapabilities.htmlUnit();
		}else{
			return null;
		}

		capabilities.setJavascriptEnabled(true);

		WebDriver driver = null;
		try {
			driver = new RemoteWebDriverWithScreenshotFunctionality(
					new URL("http://" + host + ":" + String.valueOf(port) + "/wd/hub"),
					capabilities);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		
		driver.manage().timeouts().implicitlyWait(sleepTime, sleepUnit);

		//put into cache-map
		String key = uniqueId;
		for(int i=0; alwaysCreated.containsKey(key); i++){
			key = uniqueId + "_" + String.valueOf(i);
		}
		alwaysCreated.put(key, driver);
		
		return driver;
	}
	
	private static String generateUniqueString(String host, int port, String browserMode){
		return String.valueOf(host) + "_" +  String.valueOf(port) + "_" + String.valueOf(browserMode);
	}
	
	public static void closeAll(){
		for(WebDriver driver : alwaysCreated.values()){
			try{
				driver.close();
			}catch(Exception e){}
		}
	}
}
