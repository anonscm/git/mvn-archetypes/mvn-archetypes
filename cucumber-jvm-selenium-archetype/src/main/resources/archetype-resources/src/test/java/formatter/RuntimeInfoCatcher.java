package ${package}.formatter;

import java.util.List;

import gherkin.formatter.Formatter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;

/**
 * This class is a workaround for a missing hook!
 * Because the hook which is called after each step
 * doesn't provide information about the current 
 * feature or current scenario and so on. This class
 * represents a formatter that we refer to the 
 * cucumber-cli. 
 *
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class RuntimeInfoCatcher implements Formatter {
	private static RuntimeInfoCatcher instance;
	
	private Feature currentFeature;
	private Scenario currentScenario;
	private Step currentStep;
	
	private int currentScenarioCount = 0;
	private int currentStepCount = 0;
	
	public RuntimeInfoCatcher(){
		instance = this;
	}
	
	public static RuntimeInfoCatcher getInstance(){
		return instance;
	}
	
	@Override
	public void feature(Feature feature) {
		if(!feature.equals(currentFeature)){
			currentScenarioCount = 0;
			currentStepCount = 0;
		}
		
		this.currentFeature = feature;
	}

	@Override
	public void scenario(Scenario scenario) {
		if(!scenario.equals(currentScenario)){
			currentScenarioCount++;
			currentStepCount = 0;
		}
		
		this.currentScenario = scenario;
	}

	@Override
	public void step(Step step) {
		if(!step.equals(currentStep)){
			currentStepCount++;
		}
		
		this.currentStep = step;
	}

	public Feature getCurrentFeature() {
		return currentFeature;
	}
	public Scenario getCurrentScenario() {
		return currentScenario;
	}
	public Step getCurrentStep() {
		return currentStep;
	}
	public int getCurrentScenarioCount() {
		return currentScenarioCount;
	}
	public int getCurrentStepCount() {
		return currentStepCount;
	}

	@Override
	public void background(Background arg0) {}

	@Override
	public void close() {}

	@Override
	public void done() {}

	@Override
	public void eof() {}

	@Override
	public void examples(Examples arg0) {}

	@Override
	public void scenarioOutline(ScenarioOutline arg0) {}

	@Override
	public void syntaxError(String arg0, String arg1, List<String> arg2, String arg3, Integer arg4) {}

	@Override
	public void uri(String arg0) {}
}
