package ${package}.env;

import gherkin.formatter.model.Feature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.codec.binary.Base64;

import com.thoughtworks.selenium.Selenium;

public class Utils {

	public static String generateOutputPath(String rootDir, Feature curFeature, int currentCaseCount, int currentStepCount){
		//<rootDir>/<feature-name>/case_<caseNr.>_step_<stepNr.>
		String path = rootDir;
		
		String featureName = curFeature.getName();
		//replace unusefully characters
		featureName = featureName.replaceAll("[^a-zA-Z0-9]", "_");
		
		path += featureName + "/case_" + currentCaseCount + "_step_" + currentStepCount;
		
		return path;
	}
	
	public static void deleteDir(String path) {
		File dir = new File(path);
		if (dir.exists()) {
			File[] files = dir.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDir(files[i].getAbsolutePath());
				} else {
					files[i].delete();
				}
			}
		}
		dir.delete();
	}
	
	public static void createDirectoryIfDoesntExist(String path){
		if(!path.endsWith("/")){
			path = path.substring(0, path.lastIndexOf('/'));
		}
		
		File file = new File(path);
		file.mkdirs();
	}
	
	public static void zipDirectory(String dirPath, String filePath) throws IOException{
		new File(filePath).delete();
		
		zipDirectory(new File(dirPath), new File(filePath));
	}
	
	public static final void zipDirectory(File directory, File zip)
			throws IOException {
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zip));
		zip(directory, directory, zos);
		zos.close();
	}

	private static final void zip(File directory, File base, ZipOutputStream zos)
			throws IOException {
		File[] files = directory.listFiles();
		byte[] buffer = new byte[8192];
		int read = 0;
		for (int i = 0, n = files.length; i < n; i++) {
			if (files[i].isDirectory()) {
				zip(files[i], base, zos);
			} else {
				FileInputStream in = new FileInputStream(files[i]);
				ZipEntry entry = new ZipEntry(files[i].getPath().substring(
						base.getPath().length() + 1));
				zos.putNextEntry(entry);
				while (-1 != (read = in.read(buffer))) {
					zos.write(buffer, 0, read);
				}
				in.close();
			}
		}
	}
	
	public static void writeB64ToFile(String b64, String filePath) throws IOException{
		byte[] decoded = Base64.decodeBase64(b64);
		
		writeByteArrayToFile(decoded, filePath);
	}
	
	public static void writeByteArrayToFile(byte[] content, String filePath) throws IOException{
		FileOutputStream fw = new FileOutputStream(new File(filePath));
		fw.write(content);
		fw.close();
	}
	
	public static void writeHtmlFile(String htmlSource, String filePath) throws IOException{
		FileOutputStream fw = new FileOutputStream(new File(filePath));
		fw.write(htmlSource.getBytes());
		fw.close();
	}
	
	public static String createTempFile(Selenium selenium, String directory, String content, String fileNamePrefix) throws IOException{
		if(directory == null || directory.equals("")){
			File tmp = File.createTempFile(fileNamePrefix, ".pdf");
			tmp.deleteOnExit();

			FileOutputStream os = new FileOutputStream(tmp);
			os.write(content.getBytes());
			os.flush();
			os.close();

			return tmp.getAbsolutePath();
		}else{
			String doc = directory + "/" + Long.toHexString(System.currentTimeMillis()) + ".pdf";
			//create a testfile on the remote server. this file can be used for uploading!
			selenium.captureScreenshot(doc);
			
			new File(doc).deleteOnExit();
			return doc;
		}
	}
}
