package ${package}.env;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import ${package}.CucumberJUnitRunner;
import ${package}.env.event.HandlerAfter;
import ${package}.env.event.HandlerBefore;
import ${package}.env.event.HandlerStart;
import ${package}.env.event.HandlerStop;
import ${package}.stepdefinitions.AbstractStepDefinition;

/**
 * This class contains all important (cucumber-)hooks.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class HookRegistry {
	
	private static List<HandlerAfter> afterHandler = new ArrayList<HandlerAfter>();
	private static List<HandlerBefore> beforeHandler = new ArrayList<HandlerBefore>();
	private static List<HandlerStart> startHandler = new ArrayList<HandlerStart>();
	private static List<HandlerStop> stopHandler = new ArrayList<HandlerStop>();
	
	private static boolean initialised = false;
	
	protected static boolean executeHooks;
	
	/**
	 * This constructor will be invoked by spring.
	 */
	public HookRegistry(String sExecuteHooks){
		if(CucumberJUnitRunner.isJunitTest()){
			executeHooks = CucumberJUnitRunner.isExecuteHooks();
		}else{
			executeHooks = Boolean.valueOf(sExecuteHooks);
		}
	}
	
	/**
	 * This hook will be called before cucumber starts
	 * to process features.
	 */
	@PostConstruct
	public void postConstruct() throws Exception{
		if(!initialised) initialised = true;
		else return;
		
		//initialise shutdown-hook
		if(executeHooks){
			Runtime.getRuntime().addShutdownHook(new Thread() {
			    public void run() {
		    		preDestroy();
			    }
			});
		}
		
		//forward event
		if(executeHooks){
			for(HandlerStart handler : startHandler){
				handler.handleStart();
			}
		}
	}
	
	public static class CucumberHookHost extends AbstractStepDefinition {
		/**
		 * This hook will be called before each scenario.
		 */
		@Before
		@org.junit.Before
		public void beforeScenario(){
			//forward event
			if(executeHooks){
				for(HandlerBefore handler : beforeHandler){
					handler.handleBeforeScenario();
				}
			}
		}
		
		/**
		 * This hook will be called after each scenario.
		 */
		@After
		@org.junit.After
		public void afterScenario(Scenario result){
			//forward event
			if(executeHooks){
				for(HandlerAfter handler : afterHandler){
					handler.handleAfterScenario(result);
				}
			}
		}
	}
	
	/**
	 * This hook will be called before the program will exit.
	 */
	private static void preDestroy(){
		//forward event
		for(HandlerStop handler : stopHandler){
			handler.handleStop();
		}
	}
	
	//event handler functions
	public static void addOnStartHandler(HandlerStart handler){
		startHandler.add(handler);
	}
	
	public static void addOnStopHandler(HandlerStop handler){
		stopHandler.add(handler);
	}
	
	public static void addOnAfterHandler(HandlerAfter handler){
		afterHandler.add(handler);
	}
	
	public static void addOnBeforeHandler(HandlerBefore handler){
		beforeHandler.add(handler);
	}
}
