#!/bin/bash
#
# @Author: Sven Schumann (s.schumann@tarent.de)
#
# Script that starts cucumber via cucumber-main-method
#

######
# Variables
######

CMD=`basename $0`

BLACKLISTED_MIME_TYPES="image/png"
TARGET_CLASSPATH=
JVM_ARGS=

CUCUMBER_MAIN="cucumber.api.cli.Main"
CUCUMBER_GLUE="${package}"
CUCUMBER_DEFAULT_FEATURES="./src/test/resources/features/"
CUCUMBER_DEFAULT_FORMATTER="progress"
CUCUMBER_RESULT_CONTENT_PATH="./test-results/"
CUCUMBER_RUNTIME_INFO_CATCHER="${package}.formatter.RuntimeInfoCatcher"
CUCUMBER_JSON_FORMATTER="${package}.formatter.JSONFormatter"
CUCUMBER_HTML_FORMATTER="${package}.formatter.HTMLFormatter"
CUCUMBER_ARGS=

ARG_DEBUG="false"
ARG_DEBUG_PORT="8000"
ARG_EXT_JVM_ARGS=
ARG_EXT_CUCUMBER_ARGS=

######
# Functions
######

out()
{
	echo "[INFO] $1"	
}

err()
{
	echo "[ERROR] $1"
}

checkPrecondition()
{
	for arg in $@; do
		interpretHelp $arg
	done
	
	if [ ! -f ./pom.xml ];then
		err "Could not find pom.xml"
		exit 1
	fi	
}

showHelpAndExit()
{
	echo "Author: Sven Schumann (s.schumann@tarent.de)"
	echo "-------"
	echo ""
	echo "$CMD [script-options] [cucumber-options] [ [FILE|DIR][:LINE[:LINE]*] ]+"
	echo ""
	echo "Script-Options:"
	echo ""
	echo "  --help, -help, -h           Show this help."
	echo "  --debug, --debug=true       Enable cucumber-debugging."
	echo "  --debugport=<port>          Set the java-debug port."
	echo "  --jvmargs=<arguments>       Set additional jvm arguments for cucumber."
	echo ""
	echo "Cucumber-Options:"
	echo ""
	echo "  -g, --glue PATH             Where glue code (step definitions and hooks) is loaded from."
	echo "  -f, --format FORMAT[:OUT]   How to format results. Goes to STDOUT unless OUT is specified."
	echo "                              Available formats: junit, html, pretty, progress, json, json-pretty."
	echo "  -t, --tags TAG_EXPRESSION   Only run scenarios tagged with tags matching TAG_EXPRESSION."
	echo "  -n, --name REGEXP           Only run scenarios whose names match REGEXP."
	echo "  -d, --[no-]-dry-run         Skip execution of glue code."
	echo "  -m, --[no-]-monochrome      Don't colour terminal output."
	echo "  -s, --[no-]-strict          Treat undefined and pending steps as errors."
	echo "      --dotcucumber           Where to write out runtime information."
	echo "  -v, --version               Print version."
	echo ""
	echo "Examples:"
	echo ""
	echo "Starts cucumber with the pretty formatter."
	echo "  $CMD --format pretty $CUCUMBER_DEFAULT_FEATURES"
	echo ""
	echo "Execute only scenarios with a @fail annotation."
	echo "  $CMD --tags=@fail $CUCUMBER_DEFAULT_FEATURES"
	echo ""
	echo "Execute only the scenario in Example.feature-File at line 5."
	echo "  $CMD src/test/resources/features/Example.feature:5"
	
	exit 0
}

interpretArguments()
{
	for arg in $@; do
		interpretDebug $arg
		interpretDebugPort $arg
		interpretExtJvmArguments $arg
		interpretExtCucumberArguments $arg
	done
}

interpretHelp()
{
	if [[ $1 == "--help" ]] || [[ $1 == "-help" ]] || [[ $1 == "-h" ]]; then
		showHelpAndExit
	fi	
}

interpretDebug()
{
	if [[ $1 == --debug* ]]; then
		if [[ $1 == "--debug" ]]; then
			ARG_DEBUG="true"
		else if [[ $1 == "--debug=true" ]]; then 
			ARG_DEBUG="true"
		else if [[ $1 == "--debug=false" ]]; then 
			ARG_DEBUG="false"
		fi; fi; fi
	fi	
}

interpretDebugPort()
{
	if [[ $1 == --debugport=* ]]; then
		ARG_DEBUG_PORT=`echo $1 | cut -d\= -f2`
	fi	
}

interpretExtJvmArguments()
{
	if [[ $1 == --jvmargs=* ]]; then
		ARG_EXT_JVM_ARGS=`echo $1 | cut -d\= -f2`
	fi	
}

interpretExtCucumberArguments()
{
	if [[ ! $1 == --help* ]] && [[ ! $1 == --debug* ]] && [[ ! $1 == --debugport* ]] && [[ ! $1 == --jvmargs* ]]; then
		ARG_EXT_CUCUMBER_ARGS="$ARG_EXT_CUCUMBER_ARGS $1"
	fi	
}

generateClassPath()
{
	OUTPUT_FILE="target/cp.txt"
	if [ "pom.xml" -nt $OUTPUT_FILE ]; then
		mvn --quiet dependency:build-classpath -Dmdep.outputFile=$OUTPUT_FILE
		touch $OUTPUT_FILE
	fi
	
	TARGET_CLASSPATH=`cat $OUTPUT_FILE`
}

compile()
{
	mvn clean compile test-compile	
}

buildJavaArguments()
{
	JVM_ARGS="-cp"
	JVM_ARGS="$JVM_ARGS target/classes"
	JVM_ARGS="$JVM_ARGS:target/test-classes"
	JVM_ARGS="$JVM_ARGS:$TARGET_CLASSPATH"
	
	if [[ $ARG_DEBUG == "true" ]]; then
		JVM_ARGS="$JVM_ARGS -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address="$ARG_DEBUG_PORT
	fi
	
	JVM_ARGS="$JVM_ARGS -Dtarent.cucumber.blacklist.mime.embedding=$BLACKLISTED_MIME_TYPES";
	
	JVM_ARGS="$JVM_ARGS $ARG_EXT_JVM_ARGS"
}

buildCucumberArguments()
{
	CUCUMBER_ARGS="--glue "$CUCUMBER_GLUE
	
	#formatter
	CUCUMBER_ARGS=$CUCUMBER_ARGS" --format "$CUCUMBER_JSON_FORMATTER":"$CUCUMBER_RESULT_CONTENT_PATH"/report.json"
	CUCUMBER_ARGS=$CUCUMBER_ARGS" --format "$CUCUMBER_HTML_FORMATTER":"$CUCUMBER_RESULT_CONTENT_PATH
	CUCUMBER_ARGS=$CUCUMBER_ARGS" --format "$CUCUMBER_RUNTIME_INFO_CATCHER
	
	if [[ -z "$ARG_EXT_CUCUMBER_ARGS" ]];then
		CUCUMBER_ARGS=$CUCUMBER_ARGS" --format "$CUCUMBER_DEFAULT_FORMATTER

		CUCUMBER_ARGS="$CUCUMBER_ARGS "$CUCUMBER_DEFAULT_FEATURES
	else
		if [[ ! $ARG_EXT_CUCUMBER_ARGS == *--format* ]];then
			CUCUMBER_ARGS=$CUCUMBER_ARGS" --format "$CUCUMBER_DEFAULT_FORMATTER
		fi
		
		CUCUMBER_ARGS="$CUCUMBER_ARGS $ARG_EXT_CUCUMBER_ARGS"
	fi
}

startCucumber()
{
	out "Starting cucumber..."
	java $JVM_ARGS $CUCUMBER_MAIN $CUCUMBER_ARGS
	
	if [[ $? == 0 ]];then
		out "...cucumber has successfully finished."
	else
		out "...cucumber has unsuccessfully finished."
	fi
	
	
	#exit with java(cucumber) exit-code
	exit $?
}

######
# Main
######

checkPrecondition $@
interpretArguments $@

out "Start compiling classes..."
compile
out "Gathering maven dependencies..."
generateClassPath

buildJavaArguments
buildCucumberArguments

startCucumber
