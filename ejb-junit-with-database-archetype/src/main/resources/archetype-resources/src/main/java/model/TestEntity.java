package ${package}.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class TestEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8572486076658779198L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String name;

	@ManyToOne
	@JoinColumn(name = "parent")
	TestEntity parent;

	@OneToMany(mappedBy = "parent")
	private List<TestEntity> children;

	public TestEntity() {
		super();
	}

	public TestEntity(String name) {
		super();
		setName(name);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setParent(TestEntity parent) {
		this.parent = parent;
	}

	public TestEntity getParent() {
		return parent;
	}

	public void setChildren(List<TestEntity> children) {
		this.children = children;
	}

	public List<TestEntity> getChildren() {
		return children;
	}

	@Override
	public String toString() {
		return "TestEntity [" + " id: " + getId() + " Name: " + getName()
				+ " Parent: " + getParent() + "]";
	}

}