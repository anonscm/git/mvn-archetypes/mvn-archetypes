package ${package}.service;

import javax.ejb.Local;

import ${package}.model.TestEntity;

@Local
public interface TestEntityServiceBeanLocal {

	public TestEntity getById(int id);
	
	public void add(TestEntity c);

}
