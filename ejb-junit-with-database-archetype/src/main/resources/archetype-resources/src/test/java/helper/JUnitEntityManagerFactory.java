package ${package}.helper;
 
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
 
public class JUnitEntityManagerFactory {
	private EntityManagerFactory factory;
	private EntityManager createdManager;
 
	public JUnitEntityManagerFactory(String persistenceUnitName){
		this.factory = Persistence.createEntityManagerFactory(persistenceUnitName);
	}
 
	public EntityManager getEntityManager(){
		if(createdManager == null){
			createdManager = factory.createEntityManager();
			//it's necessary to begin a transaction!
			createdManager.getTransaction().begin();
		}
 
		return createdManager;
	}
 
	@PreDestroy
	public void clean(){
		//commit or rollback
		createdManager.getTransaction().commit();
	}
}