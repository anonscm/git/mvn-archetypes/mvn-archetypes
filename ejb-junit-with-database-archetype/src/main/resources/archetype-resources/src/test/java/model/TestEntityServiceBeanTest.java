package ${package}.model;

import static org.junit.Assert.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import ${package}.model.TestEntity;
import ${package}.model.TestEntityServiceBeanTest;
import ${package}.service.TestEntityServiceBeanLocal;
import ${package}.service.impl.TestEntityServiceBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"/TestContext.xml"
	})

public class TestEntityServiceBeanTest{

	@Autowired
	private EntityManager em;
	
	private TestEntityServiceBeanLocal service;

	private TestEntity c1;
	
	private TestEntity c2;
	
	private TestEntity c3;

	private static boolean isInitialized = false;
	
	@PostConstruct
	public void setup() {
		if (isInitialized) return;
		else isInitialized = true;

		JavaEEGloss gloss = new JavaEEGloss();
		//register all dependend EJBs
		//gloss.addEJB(Mockito.mock(AnotherServiceRemote.class));

		//set EntityManager
		gloss.addGenericEM(em);

		service = gloss.make(TestEntityServiceBean.class);
		
		c1 = new TestEntity("Test1");
		c2 = new TestEntity("Test2");
		c3 = new TestEntity("Test3");
		
		c3.setParent(c2);
		c2.setParent(c1);
		
		service.add(c1);
		service.add(c2);
		service.add(c3);
	}

	@Test
	public void testGetById() {
		TestEntity t2 = service.getById(c2.getId());
		assertNotNull(t2);
		assertEquals(t2.getId(), c2.getId());
		
		TestEntity t1 = service.getById(c2.getParent().getId());
		assertNotNull(t1);
		assertEquals(t1.getId(), c1.getId());
	}

}